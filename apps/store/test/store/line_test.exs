defmodule Store.LineTest do
  use ExUnit.Case
  doctest Store.Line

  alias Store.{Line}

  test "can be instantiated with default data" do
    line = %Line{name: "test line"}

    assert line
    assert line.name   == "test line"
  end
end
