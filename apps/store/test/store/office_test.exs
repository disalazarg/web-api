defmodule Store.OfficeTest do
  use ExUnit.Case
  doctest Store.Office

  alias Store.{Office}

  test "can be instantiated with default data" do
    office = %Office{name: "test"}

    assert office
    assert office.name   == "test"
    assert office.online == false
  end
end
