defmodule StoreTest do
  use ExUnit.Case
  doctest Store

  alias Store.{Server, Office, Line}

  setup do
    Store.reset
    Store.put(Office, ["test"], %{"name" => "test office"})
  end

  test "gets basic server data" do
    server = Store.get(Server)

    assert server
    assert Map.get(server, "name") == "zeroq"
  end

  test "can create and acquire a sample office" do
    Store.put(Office, ["test"], %{"name" => "test office"})
    office = Store.get(Office, ["test"])

    assert office
    assert office.name   == "test office"
    assert office.online == false
  end

  test "can update an existing office" do
    Store.put(Office, ["test"], %{"name" => "test office"})
    Store.update(Office, ["test"], %{"online" => true})
    office = Store.get(Office, ["test"])

    assert office
    assert office.name   == "test office"
    assert office.online == true
  end

  test "can create a sample line on an existing office" do
    Store.put(Office, ["test"], %{"name" => "test office"})
    Store.put(Line, ["test", "test-line"], %{"name" => "test line"})
    line = Store.get(Line, ["test", "test-line"])

    assert line
    assert line.name == "test line"
  end

  test "can list all existing offices" do
    list = Store.list(Office, [])

    assert list
    refute list == []
    assert %{"name" => "test office"} = List.first(list)
  end
end
