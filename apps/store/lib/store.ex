defmodule Store do  
  @moduledoc """
  Documentation for Store.
  """

  def reset(), do: Store.State.reset()
  def state(), do: Store.State.state()
  def list(model, path \\ []), do: Store.State.list(model, path)
  def get(model, path \\ []), do: Store.State.get(model, path)
  def put(model, path \\ [], data \\ %{}), do: Store.State.put(model, path, data)
  def update(model, path \\ [], data \\ %{}), do: Store.State.update(model, path, data)
end
