defmodule Store.State.API do
  import Store.State.{Base, Model}
  
  defmacro __using__(state: state) do
    quote do
      import Store.State.API

      def reset(), do: GenServer.call(__MODULE__, {:reset})
      def state(), do: GenServer.call(__MODULE__, {:state})

      def list(model, path), do: GenServer.call(__MODULE__, {:list, model, path})
      def get(model, path), do: GenServer.call(__MODULE__, {:get, model, path})
      def put(model, path, data), do: GenServer.call(__MODULE__, {:put, model, path, data})
      def update(model, path, data), do: GenServer.call(__MODULE__, {:update, model, path, data})

      create_basic_handlers(unquote(state))
    end
  end

  defmacro defmodel(model, path, opts \\ [])
  defmacro defmodel(model, path, _opts) do
    args  = Enum.map(path, fn elem -> elem |> Macro.var(__MODULE__) end)
    path  = Enum.map(path, fn elem -> to_string(elem) <> "s" end)
    route = routify(path, args)
    
    quote do
      create_list(unquote(model), unquote(del_arg args), unquote(del_arg route))
      create_get(unquote(model), unquote(args), unquote(route))
      create_put(unquote(model), unquote(args), unquote(route))
      create_update(unquote(model), unquote(args), unquote(route))
    end
  end

  def routify(path, args, res \\ [])
  def routify([path | patht], [arg | argt], res), do: routify(patht, argt, res ++ [path, arg])
  def routify(path=[_hd | _tl], [], res), do: res ++ path
  def routify([], _args, res), do: res

  def del_arg(list) do
    list
    |> Enum.reverse
    |> tl
    |> Enum.reverse
  end
end
