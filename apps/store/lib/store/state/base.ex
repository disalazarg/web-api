defmodule Store.State.Base do
  defmacro create_basic_handlers(state \\ %{}) do
    quote do
      def handle_call({:reset}, _from, _state) do
        {:reply, :ok, unquote(state)}
      end

      def handle_call({:state}, _from, state) do
        {:reply, state, state}
      end

      # Helpers
      def cast(nil, _model), do: nil
      def cast(data, model) do
        data
        |> Map.take(skeys(model))
        |> (fn data ->
          for {k, v} <- data, into: %{}, do: {String.to_existing_atom(k), v}
        end).()
        |> (fn data -> struct!(model, data) end).()
      end

      def stringify(nil), do: nil
      def stringify(data) do
        data
        |> Map.from_struct
        |> (fn data ->
          for {k, v} <- data, into: %{}, do: {to_string(k), v}
        end).()
      end

      def skeys(model) do
        model
        |> struct!
        |> Map.from_struct
        |> Map.keys
        |> Enum.map(&to_string/1)
      end
    end
  end
end
