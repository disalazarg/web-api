defmodule Store.State.Model do
  defmacro create_list(model, args, route) do
    quote do
      def handle_call({:list, unquote(model), unquote(args)}, _from, state) do
        result = state
        |> get_in(unquote(route))
        |> Map.values
        
        {:reply, result, state}
      end
    end
  end

  defmacro create_get(model, args, route) do
    quote do
      def handle_call({:get, unquote(model), unquote(args)}, _from, state) do
        result = state
        |> get_in(unquote(route))
        |> cast(unquote(model))
        
        {:reply, result, state}
      end
    end
  end

  defmacro create_put(model, args, route) do
    quote do
      def handle_call({:put, unquote(model), unquote(args), data}, _from, state) do
        data = data
        |> cast(unquote(model))
        |> stringify
        
        state = state
        |> put_in(unquote(route), data)
        
        {:reply, :ok, state}
      end
    end
  end

  defmacro create_update(model, args, route) do
    quote do
      def handle_call({:update, unquote(model), unquote(args), data}, _from, state) do
        data = data
        |> Map.take(skeys(unquote(model)))

        state = state
        |> update_in(unquote(route), fn origin ->
          Map.merge(origin, data)
        end)

        {:reply, data, state}
      end
    end
  end
end
