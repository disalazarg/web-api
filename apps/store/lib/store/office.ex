defmodule Store.Office do
  defstruct name: nil, online: false, lines: %{}
end
