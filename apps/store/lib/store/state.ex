defmodule Store.State do
  @base_state %{"server" => %{"name" => "zeroq", "version" => "5.0-alpha"}, "offices" => %{}}
  
  use GenServer
  use Store.State.API, state: @base_state

  alias Store.{Server, Office, Line}
  
  # Constructor
  def init(initial_state) do
    state = @base_state
    |> Map.merge(initial_state)
    
    {:ok, state}
  end
  
  def start_link(initial_state), do: GenServer.start(__MODULE__, initial_state, name: __MODULE__)

  defmodel Office, [:office]
  defmodel Line,   [:office, :line]

  def handle_call({:get, Server, []}, _from, state) do
    result = state
    |> Map.get("server")
    
    {:reply, result, state}
  end
end
