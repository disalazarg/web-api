defmodule API.ServerControllerTest do
  use API.ConnCase

  setup %{conn: conn} do
    Store.reset
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end
  
  test "shows chosen resource", %{conn: conn} do
    conn = get conn, server_path(conn, :index)
    assert json_response(conn, 200)["data"] == %{"name" => "zeroq", "version" => "5.0-alpha"}
  end
end
