defmodule API.OfficeControllerTest do
  use API.ConnCase

  setup %{conn: conn} do
    Store.reset
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  test "it can list all resources", %{conn: conn} do
    conn = get conn, office_path(conn, :index)
    
    assert json_response(conn, 200)["data"] == []
  end

  test "can create a new resource given valid parameters", %{conn: _conn} do
  end
end
