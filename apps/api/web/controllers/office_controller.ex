defmodule API.OfficeController do
  use API.Web, :controller

  alias Store.Office

  def index(conn, _params) do
    offices = Store.list(Office, [])

    render(conn, "index.json", offices: offices)
  end

  def create(conn, _params) do
    office = Store.put(Office, ["test"], %{"name" => "test office"})

    render(conn, "show.json", office: office)
  end
end
