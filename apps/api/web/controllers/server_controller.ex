defmodule API.ServerController do
  use API.Web, :controller

  alias Store.Server

  def index(conn, _params) do
    server = Store.get(Server, [])
    render(conn, "index.json", server: server)
  end
end
