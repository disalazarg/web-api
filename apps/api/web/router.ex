defmodule API.Router do
  use API.Web, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", API do
    pipe_through :api

    get "/", ServerController, :index
    resources "/offices", OfficeController, except: [:new, :edit]
  end
end
