defmodule API.OfficeView do
  use API.Web, :view

  def render("index.json", %{offices: offices}) do
    %{data: render_many(offices, API.OfficeView, "office.json")}
  end

  def render("show.json", %{office: office}) do
    %{data: render_one(office, API.OfficeView, "office.json")}
  end

  def render("office.json", %{office: office}) do
    office
  end
end
