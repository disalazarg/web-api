defmodule API.ServerView do
  use API.Web, :view

  def render("index.json", %{server: server}) do
    %{data: render_one(server, API.ServerView, "server.json")}
  end

  def render("show.json", %{server: server}) do
    %{data: render_one(server, API.ServerView, "server.json")}
  end

  def render("server.json", %{server: server}) do
    server
  end
end
